package TestRunner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features/",
tags="@VerifyCartItems",glue= {"StepDefinitions"})
public class RunnerTest {

}
