package com.org.webAutomation;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Amazon1 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.in/");
		driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[5]")).click();

		// Select brand [3]Redmi,[4]Boat,[5]Oneplus,[6]Samsung,[7]Realme,[8]MI,[9]JBL
		WebElement target = driver.findElement(By.xpath("(//*[@class='a-size-base a-color-base'])[6]"));// [6]Samsung
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", target);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		target.click();

		String winBefore = driver.getWindowHandle();
		driver.findElement(By.xpath("(//img[@class='s-image'])[1]")).click();// Select items from [1] to [24]
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// Add to cart
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='add-to-cart-button']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.findElement(By.xpath("//*[@id=\'attach-sidesheet-view-cart-button\']/span/input")).click();
		} catch (NoSuchElementException e) {
			driver.findElement(By.xpath("//*[@id=\'hlb-view-cart-announce\']"));
		}

		driver.close();
		driver.switchTo().window(winBefore);
		driver.findElement(By.id("nav-cart-text-container")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		int cartItems = driver.findElements(By.xpath("//*[@class='a-truncate-cut']")).size();
		if (cartItems != 0) {
			if (cartItems >= 2) {
				System.out.println("Selected " + cartItems + " are added to the cart succesfully!");

			} else {
				System.out.println("Selected " + cartItems + " item is added to the cart succesfully!");
			}

		} else {
			System.out.println("The cart is empty!!");
		}
		driver.quit();

	}

}
